class CreateHosts < ActiveRecord::Migration[5.1]
  def change
    create_table :hosts do |t|
      t.string :name
      t.boolean :working
      t.integer :period

      t.timestamps
    end
  end
end

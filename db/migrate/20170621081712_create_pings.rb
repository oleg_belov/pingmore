class CreatePings < ActiveRecord::Migration[5.1]
  def change
    create_table :pings do |t|
      t.references :host, foreign_key: true
      t.float :rtt_min
      t.float :rtt_avg
      t.float :rtt_max
      t.integer :sent
      t.integer :received
      t.datetime :start_time
      #
      # t.timestamps
    end
    add_index :pings, :start_time
  end
end

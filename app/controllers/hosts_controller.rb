class HostsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:stats]

  def index
    run Host::Index
    render cell(Host::Cell::Index, result['model'], context: { notice: notice })
  end

  def show
    run Host::Show
    render cell(Host::Cell::Show, result['model'], context: { notice: notice })
  end

  def new
    run Host::Create::Present
    render cell(Host::Cell::New, result['model'])
  end

  def edit
    run Host::Update::Present
    render cell(Host::Cell::Edit, result['model'])
  end

  def create
    run Host::Create do |result|
      return redirect_to host_path(result['model']), notice: 'Host was successfully created.'
    end
    render cell(Host::Cell::New, result['model'])
  end

  def update
    run Host::Update do |result|
      return redirect_to host_path(result['model']), notice: 'Host was successfully updated.'
    end
    render cell(Host::Cell::Edit, result['model'])
  end

  def destroy
    run Host::Destroy
    redirect_to hosts_url, notice: 'Host was successfully destroyed.'
  end

  def stats
    respond_to do |format|
      format.html { redirect_to root }
      format.json do
        result = Host::Stats.(host: params[:host], start: params[:start], end: params[:end])
        render :json => result['model']
      end
    end
  end
end

module Host::Cell
  class Show < Trailblazer::Cell
    def notice
      context[:notice]
    end
  end
end
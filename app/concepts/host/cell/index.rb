module Host::Cell
  class Index < Trailblazer::Cell
    def notice
      context[:notice]
    end
  end
end
class Host::Contract::Create < Reform::Form
  property :name
  property :working
  property :period

  validates :name, presence: true
  validates :working, inclusion: {in: %w(0 1)}, presence: true
  validates :period, inclusion: {in: %w(5 15)}, presence: true
end
class Host::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Host, :new)
    step Contract::Build(constant: Host::Contract::Create)
  end

  step Nested(Present)
  step Contract::Validate(key: :host)
  step Contract::Persist()
end
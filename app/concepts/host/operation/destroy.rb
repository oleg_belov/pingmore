class Host::Destroy < Trailblazer::Operation
  step Model(Host, :find_by)
  step :destroy!

  def destroy!(model:, **)
    model.destroy
  end
end
class Host::Index < Trailblazer::Operation
  step :model!

  def model!(options, *)
    options['model'] = ::Host.all
  end
end
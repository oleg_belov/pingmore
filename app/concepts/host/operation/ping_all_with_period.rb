class Host::PingAllWithPeriod < Trailblazer::Operation
  step :ping_hosts

  def ping_hosts(params:, **)
    hosts = Host.where('working = ? AND period = ?', true, params).pluck(:name)

    pool = Concurrent::FixedThreadPool.new(10)

    hosts.each do |host|
      Concurrent::Future.execute(:executor => pool) {ping(host)}
    end

    pool.shutdown
    pool.wait_for_termination
  end

  def ping(host)
    start_time = Time.now
    output = `ping #{host} -c1`
    rtt_min, rtt_avg, rtt_max, stddev = output.scan(/.*?=\s([^\/]*)\/([^\/]*)\/([^\/]*)\/(.*)\sms/)[0]
    sent = output.scan(/.*?(\d+)\spackets\stransmitted/)[0][0]
    received = output.scan(/.*?(\d+)\spackets\sreceived/)[0][0]

    host = Host.find_by_name(host)
    ping = host.pings.new
    ping.rtt_min = rtt_min
    ping.rtt_avg = rtt_avg
    ping.rtt_max = rtt_max
    ping.sent = sent
    ping.received = received
    ping.start_time = start_time
    ping.save
  end
end
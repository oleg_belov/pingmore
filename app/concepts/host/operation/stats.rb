class Host::Stats < Trailblazer::Operation
  step :render_json

  def render_json(options, params:, **)
    params[:start] ||= Time.now - 1.day
    params[:end] ||= Time.now
    host     = Host.find_by_name(params[:host])
    ping_ids = host.pings.where(start_time: params[:start]..params[:end]).ids
    rtt_avg  = Ping.where(id: ping_ids).average(:rtt_avg).to_f.round(3)
    rtt_min  = Ping.where(id: ping_ids).minimum(:rtt_min)
    rtt_max  = Ping.where(id: ping_ids).maximum(:rtt_max)
    sent     = Ping.where(id: ping_ids).pluck(:sent).sum
    received = Ping.where(id: ping_ids).pluck(:received).sum
    packet_loss = (sent.to_f - received.to_f) / sent.to_f * 100
    options['model'] = {rtt_avg: rtt_avg, rtt_min: rtt_min, rtt_max: rtt_max, packet_loss: packet_loss }
  end
end
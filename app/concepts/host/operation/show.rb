class Host::Show < Trailblazer::Operation
  step Model(Host, :find_by)
end
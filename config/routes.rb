Rails.application.routes.draw do
  root to: 'hosts#index'
  resources :hosts
  match 'hosts/stats', to: 'hosts#stats', via: [:post], defaults: { :format => 'json' }
end
